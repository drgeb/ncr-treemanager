/**
 * 
 */
package com.ncr.treemanager.exception;

import com.ncr.treemanager.model.Node;

/**
 * @author drgeb
 *
 */
public class MaxChildenArraySizeExceeded extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6690946718457236338L;

	public MaxChildenArraySizeExceeded() {
		super("Exceeded number of allowed children: " + Node.MAX_NODE_SIZE);
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
