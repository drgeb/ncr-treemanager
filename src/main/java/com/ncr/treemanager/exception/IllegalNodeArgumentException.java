/**
 * 
 */
package com.ncr.treemanager.exception;

/**
 * @author drgeb
 *
 */
// DataAccessException
public class IllegalNodeArgumentException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3420058767368517376L;

	public IllegalNodeArgumentException(String errorString) {
		super(errorString);
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
