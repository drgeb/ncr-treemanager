/**
 * 
 */
package com.ncr.treemanager.exception;

/**
 * @author drgeb
 *
 */
// DataIntegrityViolationException
public class DeleteRootNodeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6336616343012551832L;

	public DeleteRootNodeException() {
		super("Not allowed to delete root node!");
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
