/**
 * 
 */
package com.ncr.treemanager.exception;

/**
 * @author drgeb
 *
 */
public class NodeNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7286629611577168986L;

	public NodeNotFoundException(String name) {
		super("Could not find node '" + name + "'.");
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}
}
