/**
 * 
 */
package com.ncr.treemanager.model;

import java.util.List;

/**
 * @author drgeb
 *
 *         Component
 */
public interface Node {
  final static int MAX_NODE_SIZE = 15;
  final static String ROOT_NODE_NAME = "root";
  final static String ROOT_NODE_DESCRIPTION = "This is the description for the root node.";

  String getName();
  // Name is unique to the Node only able to set it via Constructor for now
  // This is a design decision

  String getDescription();

  void setDescription(String description);

  String getData();

  void setData(String data);

  /**
   * @return
   */
  List<Node> getChildren();

  /**
   * @return
   */
  Node getParent();

  /**
   * @param node
   */
  void deleteChild(Node node);

  /**
   * 
   */
  void deleteAllChildren();

  /**
   * 
   */
  void addChild(Node node);

}
