/**
 * 
 */
package com.ncr.treemanager.model;

import java.util.ArrayList;

import com.ncr.treemanager.exception.MaxChildenArraySizeExceeded;

import lombok.Data;

/**
 * @author drgeb
 * 
 *         Leaf
 */
@Data(staticConstructor = "of")
public class NodeImpl implements Node {

	// Made name final cause its not allowed to be changed need to validate that
	// its unique during creation!
	private final String name;
	private String description;
	private String data;
	private Node parent = null;
	private ArrayList<Node> children = new ArrayList<Node>();

	/**
	 * @param name
	 * @param description
	 * @param data
	 */
	public NodeImpl(String name, String description, String data) {
		this.name = name;
		this.description = description;
		this.data = data;
		this.parent = null;
	}

	/**
	 * @param name
	 * @param description
	 */
	public NodeImpl(String name, String description) {
		this.name = name;
		this.description = description;
		this.data = null;
		this.parent = null;
	}

	/**
	 * Simple implementation for adding a child only checks to see if one
	 * already has MAX_NODE_SIZE of children only
	 * 
	 * @param child
	 * @throws Exception
	 */
	public void addChild(Node child) throws MaxChildenArraySizeExceeded {
		if (children.size() < Node.MAX_NODE_SIZE) {
			((NodeImpl) child).setParent(this);
			children.add(child);
		} else {
			throw new MaxChildenArraySizeExceeded();
		}
	}

	/**
	 * Simple implementation for deletion of a child This should only be done
	 * through a Manager which keeps track if its a root Node which is not
	 * allowed and deletion of all descendants nodes.
	 * 
	 * @param n
	 * @throws Exception
	 */
	public void deleteChild(Node child) {
		children.remove(child);
	}

	public void deleteAllChildren() {
		children.clear();
	}
}
