/**
 * 
 */
package com.ncr.treemanager.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.ncr.treemanager.exception.DeleteRootNodeException;
import com.ncr.treemanager.exception.IllegalNodeArgumentException;
import com.ncr.treemanager.exception.NodeNotFoundException;

/**
 * @author drgeb *
 * 
 *         Composite
 *
 */
@Repository
public class NodeManager {
	// Keep track of all names used for all nodes to make sure they are unique
	private static final HashMap<String, Node> nodeRegistry = new HashMap<String, Node>();

	// The root Node which must exist at all times and can not be deleted!
	private static final Node root = new NodeImpl(Node.ROOT_NODE_NAME, Node.ROOT_NODE_DESCRIPTION, null);

	// Make this class a Singleton!
	// This implementation is not thread safe need to think about this
	private static volatile NodeManager nm = null;

	public NodeManager() {
		nodeRegistry.put(Node.ROOT_NODE_NAME, root);
	}

	public static NodeManager getInstance() {
		if (nm == null) {
			synchronized (NodeManager.class) {
				if (nm == null) {
					nm = new NodeManager();
				}
			}
		}
		return nm;
	}

	// Can make this static but forcing implementers to use the NodeManager
	// instance!
	public Node getRootNode() {
		return root;
	}

	public void addNode(String parent, Node child) throws IllegalNodeArgumentException, NodeNotFoundException {
		Node p = nodeRegistry.get(parent);
		if (p == null) {
			throw new NodeNotFoundException(parent);
		}
		if (child == null) {
			throw new NodeNotFoundException("<null>");
		}
		addNode(p, child);
	}

	// Add a node to the tree at a specific location (for instance, add a new
	// node to a leaf node’s children)
	public void addNode(Node parent, Node child) throws IllegalNodeArgumentException, NodeNotFoundException {
		String key = child.getName();
		if (nodeRegistry.containsKey(key)) {
			throw new IllegalNodeArgumentException("Node " + key + " is not unique");
		} else if (parent == null) {
			throw new NodeNotFoundException("<null>");
		} else {
			((NodeImpl) parent).addChild(child);
			nodeRegistry.put(key, child);// Add Node to HashMap!
		}
	}

	// Retrieve a single node
	public Node retrieveNode(String nodename) throws IllegalNodeArgumentException {
		Node node = nodeRegistry.get(nodename);
		if (node == null) {
			throw new IllegalNodeArgumentException("Node " + nodename + " does not exist.");
		}
		return node;
	}

	// Retrieve the immediate children of a node
	public List<Node> retrieveChildren(String nodename) {
		Node node = nodeRegistry.get(nodename);
		return retrieveChildren(node);
	}

	List<Node> retrieveChildren(Node node) {
		return node.getChildren();
	}

	// Retrieve all descendants of a node (immediate children and nested
	// children)
	public List<Node> retrieveDescendants(String nodename) {
		Node parent = nodeRegistry.get(nodename);
		if (parent == null) {
			throw new IllegalNodeArgumentException("Node " + nodename + " does not exist.");
		}
		return retrieveDescendants(parent);
	}

	/**
	 * @param node
	 * @return
	 */
	private List<Node> retrieveDescendants(Node node) {
		ArrayList<Node> allDescendants = new ArrayList<Node>();

		List<Node> children = node.getChildren();
		for (Node child : children) {
			allDescendants.add(child);

			List<Node> nodes = retrieveDescendants(child.getName());
			for (Node cnode : nodes) {
				allDescendants.add(cnode);
			}
		}
		return allDescendants;
	}

	// For an arbitrary node, retrieve all ancestors/parents of the node (the
	// path from the root node to the specific node)
	public List<Node> retrieveAncestors(String nodename) {
		ArrayList<Node> allAncestors = new ArrayList<Node>();
		if (nodename != null) {
			Node node = nodeRegistry.get(nodename);
			if (node != null) {
				Node parent = node.getParent();
				if (parent != null) {
					allAncestors.add(parent);
					List<Node> list = retrieveAncestors(parent.getName());
					allAncestors.addAll(list);
				}
			} else {
				// TODO: Not sure if I should throw an Exception here
			}
		} else {
			// TODO: Not sure if I should throw an Exception here
		}
		return allAncestors;
	}

	// Remove a node from the tree (also removes all of its children)
	public void removeNode(Node node) throws DeleteRootNodeException {
		// First find all descendants
		if (node.equals(root) || node.getName().equals(Node.ROOT_NODE_NAME)) {
			throw new DeleteRootNodeException();
		}
		List<Node> descendants = retrieveDescendants(node);
		for (Node descendant : descendants) {
			Node parent = descendant.getParent();
			parent.deleteChild(descendant);
			nodeRegistry.remove(descendant.getName());// Remove descendant Node
		}

		Node parent = node.getParent();
		parent.deleteChild(node);
		nodeRegistry.remove(node.getName());// Remove node from the tree
	}

	// Remove a node from the tree (also removes all of its children)
	public void removeNode(String name) {
		Node node = nodeRegistry.get(name);
		if (node == null) {
			throw new IllegalNodeArgumentException("Node " + name + " does not exist.");
		}
		removeNode(node);
	}

	/**
	 * @return
	 */
	public void reset() {
		nodeRegistry.clear();
		nodeRegistry.put(Node.ROOT_NODE_NAME, root);

		// remove all children from Node.
		root.deleteAllChildren();
	}
}
