/**
* 
*/
package com.ncr.treemanager.service;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.ncr.treemanager.model.Node;
import com.ncr.treemanager.service.dto.NodeDto;

/**
 * @author drgeb
 *
 */
/// @Mapper(componentModel = "cdi")

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public abstract class NodeMapper {

  // public static NodeMapper MAPPER = Mappers.getMapper(NodeMapper.class);

  abstract List<NodeDto> nodesToNodeDtos(List<Node> nodes);

  // NOT USED abstract List<Node> nodesDtoToNodes(List<NodeDto> nodes);

  @Mappings({@Mapping(source = "name", target = "name"),
      @Mapping(source = "description", target = "description"),
      @Mapping(source = "data", target = "data")})
  public NodeDto nodeToNodeDto(Node value) {
    NodeDto nodeDto = new NodeDto(null, null, null);
    nodeDto.setName(value.getName());
    nodeDto.setDescription(value.getDescription());
    nodeDto.setData(value.getData());
    return nodeDto;
  }
}
