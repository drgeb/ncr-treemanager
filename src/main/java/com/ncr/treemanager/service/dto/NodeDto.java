/**
 * 
 */
package com.ncr.treemanager.service.dto;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author drgeb
 *
 */
@Data
@Builder
@AllArgsConstructor
@ApiModel(value = "Node", description = "Node contains the name, description and data attributes")
public class NodeDto extends ResourceSupport {
  @ApiModelProperty(
      value = "A unique name for the node. The name \"root\" is already used by default fot the root node.")
  private String name;

  @JsonView(NodeView.Summary.class)
  @ApiModelProperty(value = "A description for your node.")
  private String description;
  @JsonView(NodeView.Summary.class)
  @ApiModelProperty(value = "Data field for your node.")
  private String data;
}
