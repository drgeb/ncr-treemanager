/**
 * 
 */
package com.ncr.treemanager.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ncr.treemanager.model.Node;
import com.ncr.treemanager.model.NodeImpl;
import com.ncr.treemanager.model.NodeManager;
import com.ncr.treemanager.service.dto.NodeDto;

import io.swagger.annotations.Api;

/**
 * @author drgeb
 *
 */
@Service
@Api
public class NodeService {

  private NodeManager nodeManager;

  private NodeMapper mapper;

  @Autowired
  public NodeService(NodeManager nodeManager, NodeMapper mapper) {
    this.nodeManager = nodeManager;
    this.mapper = mapper;
  }

  // Add a node to the tree at a specific location (for instance, add a new
  /**
   * @param parentName
   * @param node
   */
  public NodeDto addNode(String parentName, NodeDto nodeDto) {
    Node node = new NodeImpl(nodeDto.getName(), nodeDto.getDescription(), nodeDto.getData());
    this.nodeManager.addNode(parentName, node);
    NodeDto newNodeDto = mapper.nodeToNodeDto(node);
    return newNodeDto;
  }

  // Retrieve a single node
  public NodeDto getSingleNode(String name) {
    Node node = this.nodeManager.retrieveNode(name);
    return mapper.nodeToNodeDto(node);
  }

  // node to a leaf node’s children)
  public List<NodeDto> getChildren(String name) {
    Node node = this.nodeManager.retrieveNode(name);
    List<Node> children = node.getChildren();
    List<NodeDto> childrenDto = mapper.nodesToNodeDtos(children);
    return childrenDto;
  }

  // Retrieve the immediate children of a node

  // Retrieve all descendants of a node
  // (immediate children and nested children)
  /**
   * @param name
   * @return
   */
  public List<NodeDto> getDescendants(String name) {
    List<Node> descendants = this.nodeManager.retrieveDescendants(name);
    List<NodeDto> descendantsDto = mapper.nodesToNodeDtos(descendants);
    return descendantsDto;
  }

  // For an arbitrary node, retrieve all ancestors/parents of the node (the
  // path from the root node to the specific node).
  /**
   * @param name
   * @return
   */
  public List<NodeDto> getAncestors(String name) {
    List<Node> ancestors = this.nodeManager.retrieveAncestors(name);
    List<NodeDto> AncestorsDto = mapper.nodesToNodeDtos(ancestors);
    return AncestorsDto;
  }

  // Remove a node from the tree (also removes all of its children)
  /**
   * @param name
   */
  public void removeNode(String name) {
    this.nodeManager.removeNode(name);
  }

}
