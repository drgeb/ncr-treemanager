/**
 * 
 */
package com.ncr.treemanager.web.application;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.ncr.treemanager.exception.DeleteRootNodeException;
import com.ncr.treemanager.exception.IllegalNodeArgumentException;
import com.ncr.treemanager.exception.MaxChildenArraySizeExceeded;
import com.ncr.treemanager.exception.NodeNotFoundException;
import com.ncr.treemanager.web.application.exception.BaseException;
import com.ncr.treemanager.web.application.exception.TreeManagerExceptionJSONInfo;

import lombok.extern.slf4j.Slf4j;

/**
 * @author drgeb
 *
 */
@RestControllerAdvice
@Slf4j
public class NodeRestControllerAdvice extends BaseException {

  public NodeRestControllerAdvice() {
    super(log);
  }

  // @RequestHandler methods
  // For codes: See https://www.npmjs.com/package/http-status-codes

  // Exception handling methods
  @ResponseStatus(value = HttpStatus.CONFLICT) // 409
  @ExceptionHandler(DeleteRootNodeException.class)
  @ResponseBody
  public TreeManagerExceptionJSONInfo conflict(final HttpServletRequest req,
      final DeleteRootNodeException ex) {
    log.error(
        "Request: " + req.getRequestURL() + "caught DeleteRootNodeException" + " raised " + ex);
    String reason = "Data integrity violation, not allowed to delete root node!";
    return createTreeManagerExceptionJSONInfo(reason, req, ex);
  }

  @ExceptionHandler(MaxChildenArraySizeExceeded.class)
  @ResponseStatus(value = HttpStatus.INSUFFICIENT_STORAGE) // 507
  @ResponseBody
  public TreeManagerExceptionJSONInfo maxSizeError(final HttpServletRequest req,
      final MaxChildenArraySizeExceeded ex, HttpServletResponse response) {
    log.error("Request: " + req.getRequestURL() + "caught: MaxChildenArraySizeExceeded" + " raised "
        + ex);
    String reason = "Data integrity violation, maxmimum child size reached!";
    return createTreeManagerExceptionJSONInfo(reason, req, ex);
  }

  @ResponseStatus(value = HttpStatus.NOT_FOUND) // 404
  @ExceptionHandler(NodeNotFoundException.class)
  @ResponseBody
  public TreeManagerExceptionJSONInfo notFoundError(final HttpServletRequest req,
      final NodeNotFoundException ex) {
    log.error(
        "Request: " + req.getRequestURL() + "caught: NodeNotFoundException" + " raised " + ex);
    String reason = "Not found violation unable to find node in registry";
    return createTreeManagerExceptionJSONInfo(reason, req, ex);
  }

  @ResponseStatus(value = HttpStatus.BAD_REQUEST) // 400
  @ExceptionHandler(IllegalNodeArgumentException.class)
  @ResponseBody
  public TreeManagerExceptionJSONInfo handleIllegalNodeArgumentException(
      final HttpServletRequest req, final IllegalNodeArgumentException ex,
      final HttpServletResponse response) {
    log.error("Request: " + req.getRequestURL() + " raised " + ex);
    String reason = "Data integrity violation,nodename issue.";
    return createTreeManagerExceptionJSONInfo(reason, req, ex);
  }

  private TreeManagerExceptionJSONInfo createTreeManagerExceptionJSONInfo(final String reason,
      final HttpServletRequest req, final Exception ex) {
    TreeManagerExceptionJSONInfo jsonInfo = new TreeManagerExceptionJSONInfo();
    jsonInfo.setCause(reason);
    String message = "INTERNAL_SERVER_ERROR";
    jsonInfo.setException(ex);
    jsonInfo.setMessage(message);
    return jsonInfo;
  }
}
