/**
 * 
 */
package com.ncr.treemanager.web.application.exception;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Data;

/**
 * @author drgeb
 *
 */
@Data
public abstract class BaseException {
	/**
	 * @param log
	 */
	Logger log;

	public BaseException(Logger log) {
		this.log = log;
	}

	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	@ExceptionHandler(Throwable.class)
	@ResponseBody
	public TreeManagerExceptionJSONInfo handleThrowable(final Throwable ex) {
		log.error("Unexpected error: " + ex);
		TreeManagerExceptionJSONInfo jsonInfo = new TreeManagerExceptionJSONInfo();
		jsonInfo.setMessage("INTERNAL_SERVER_ERROR");
		jsonInfo.setCause("An unexpected internal server error occured");
		jsonInfo.setException(ex);
		return jsonInfo;
	}
}
