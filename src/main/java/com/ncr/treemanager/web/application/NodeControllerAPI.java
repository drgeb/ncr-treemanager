/**
 * 
 */
package com.ncr.treemanager.web.application;

/**
 * @author drgeb
 *
 */
public interface NodeControllerAPI {
	final static String TREEMANAGER_MAIN_RESOURCE = "/treemanager/api";

	final static String NODENAME_REQUEST_STRING = "/{name}";
	final static String NODE_REST_ENDPOINT = "/node";
	final static String CHILDREN_REST_ENDPOINT = "/children";
	final static String DESCENDANTS_REST_ENDPOINT = "/descendants";
	final static String ANCESTORS_REST_ENDPOINT = "/ancestors";

	final static String NODE_REST_STRING = NODE_REST_ENDPOINT + NODENAME_REQUEST_STRING;
	final static String CHILDREN_REST_STRING = CHILDREN_REST_ENDPOINT + NODENAME_REQUEST_STRING;
	final static String DESCENDANTS_REST_STRING = DESCENDANTS_REST_ENDPOINT + NODENAME_REQUEST_STRING;
	final static String ANCESTORS_REST_STRING = ANCESTORS_REST_ENDPOINT + NODENAME_REQUEST_STRING;
}
