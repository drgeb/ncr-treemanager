/**
 * 
 */
package com.ncr.treemanager.web.application.exception;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 * @author drgeb
 *
 */
@Data
public class TreeManagerExceptionJSONInfo {
  private Throwable exception;
  private String message;
  private String cause;

  private String userMessage;
  private String internalMessage;
  private int code;
  private String moreInfo;

  @Setter(AccessLevel.NONE)
  private String currentTimeMillis = Long.toString(System.currentTimeMillis());

  public TreeManagerExceptionJSONInfo() {

  }
}
