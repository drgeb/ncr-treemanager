/**
 * 
 */
package com.ncr.treemanager.web.application;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ncr.treemanager.model.Node;
import com.ncr.treemanager.service.NodeService;
import com.ncr.treemanager.service.dto.NodeDto;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author drgeb
 * 
 *         Note: For all methods annotated with @RequestMapping exceptions are
 *         not caught by the method itself these are incercepted by the
 *         NodeREstControllerAdvice and handlred there accordingly!
 * 
 */
@RestController
@RequestMapping(value = NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE)
@Slf4j
public class NodeController {

	@Autowired
	private NodeService nodeService;

	// Add a node to the tree at a specific location (for instance, add a new
	// node to a leaf node’s children)
	@RequestMapping(method = RequestMethod.PUT, value = NodeControllerAPI.NODE_REST_STRING)
	public NodeDto addNodeToTree(@PathVariable(value = "name") String name, @RequestBody NodeDto node) {
		log.info("Add node: " + node.getName() + " to the tree at a specific location: " + name);
		NodeDto newNode = this.nodeService.addNode(name, node);
		addALL_HATEOAS_Links(newNode);
		return newNode;
	}

	/**
	 * 
	 */
	private void addALL_HATEOAS_Links(NodeDto node) {
		// Add Spring MVC Hateoas 1. Link to SELF
		node.add(linkTo(methodOn(NodeController.class).getSingleNode(node.getName())).withSelfRel());

		// Add Spring MVC Hateoas 2. Link to descendants, except leaf nodes!
		// TODO skip over leaf nodes!
		// Add isLeaf method to NodeDto and Node model layer
		node.add(linkTo(methodOn(NodeController.class).getDescendants(node.getName()))
				.withRel(NodeControllerAPI.DESCENDANTS_REST_ENDPOINT));

		// Add Spring MVC Hateoas 3. Link to ancestors, except root node
		if (!node.getName().equals(Node.ROOT_NODE_NAME)) {
			node.add(linkTo(methodOn(NodeController.class).getAncestors(node.getName()))
					.withRel(NodeControllerAPI.ANCESTORS_REST_ENDPOINT));
		}

		// Add Spring MVC Hateoas 4. Link to children, except leaf nodes!
		// TODO skip over leaf nodes!
		node.add(linkTo(methodOn(NodeController.class).getImmediateChildren(node.getName()))
				.withRel(NodeControllerAPI.CHILDREN_REST_ENDPOINT));
	}

	private void addSELF_HATEOAS_Link(NodeDto node) {
		// Add Spring MVC Hateoas 1. Link to SELF
		node.add(linkTo(methodOn(NodeController.class).getSingleNode(node.getName())).withSelfRel());
	}

	// Retrieve the immediate children of a node
	@RequestMapping(method = RequestMethod.GET, value = NodeControllerAPI.CHILDREN_REST_STRING)
	public List<NodeDto> getImmediateChildren(@PathVariable(value = "name") String name) {
		log.info("Retrieve the immediate children of a node: " + name);
		List<NodeDto> children = this.nodeService.getChildren(name);
		// There are no links for the list itsef Spring MVC Hateoas Links
		// But for each of the Nodes we can iterate and create self,
		// ancestors, children, and descendants links
		// To keep it clean only adding self.
		for (NodeDto child : children) {
			addSELF_HATEOAS_Link(child);
		}
		return children;
	}

	// Retrieve a single node
	@RequestMapping(method = RequestMethod.GET, value = NodeControllerAPI.NODE_REST_STRING)
	public HttpEntity<NodeDto> getSingleNode(@PathVariable(value = "name") String name) {
		log.info("Retrieve a single node: " + name);
		NodeDto nodeDto = this.nodeService.getSingleNode(name);
		// Add Spring MVC Hateoas Links
		addALL_HATEOAS_Links(nodeDto);
		return new ResponseEntity<NodeDto>(nodeDto, HttpStatus.OK);
	}

	// Retrieve all descendants of a node
	// (immediate children and nested children)
	@RequestMapping(method = RequestMethod.GET, value = NodeControllerAPI.DESCENDANTS_REST_STRING)
	public List<NodeDto> getDescendants(@PathVariable(value = "name") String name) {
		log.info("Retrieve all descendants of a node: " + name);
		List<NodeDto> descendants = this.nodeService.getDescendants(name);
		// There are no links for the list itsef Spring MVC Hateoas Links
		// But for each of the Nodes we can iterate and create self, decendants,
		// ancestors, children, and descendants links
		// To keep it clean only adding self.
		for (NodeDto descendant : descendants) {
			addSELF_HATEOAS_Link(descendant);
		}
		return descendants;
	}

	// For an arbitrary node, retrieve all ancestors/parents of the node
	// (the path from the root node to the specific node).
	@RequestMapping(method = RequestMethod.GET, value = NodeControllerAPI.ANCESTORS_REST_STRING)
	public List<NodeDto> getAncestors(@PathVariable(value = "name") String name) {
		log.info("Retrieve all ancestors of a node: " + name);
		List<NodeDto> ancestors = this.nodeService.getAncestors(name);
		// There are no links for the list itsef Spring MVC Hateoas Links
		// But for each of the Nodes we can iterate and create self, decendants,
		// ancestors, children, and descendants links
		// To keep it clean only adding self.
		for (NodeDto ancestor : ancestors) {
			addSELF_HATEOAS_Link(ancestor);
		}
		return ancestors;
	}

	// Remove a node from the tree
	// (also removes all of its children)
	@RequestMapping(method = RequestMethod.DELETE, value = NodeControllerAPI.NODE_REST_STRING)
	public void removeNode(@PathVariable(value = "name") String name) {
		log.info("Remove a node from the tree: " + name);
		this.nodeService.removeNode(name);
	}
}
