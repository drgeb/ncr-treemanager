package com.ncr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.ncr.util.SwaggerConfigurationUtil;

@SpringBootApplication
@Import({ SwaggerConfigurationUtil.class })
public class TreeManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TreeManagerApplication.class, args);
	}
}
