/**
 * 
 */
package com.ncr.treemanager.unit.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import com.ncr.treemanager.model.Node;
import com.ncr.treemanager.model.NodeImpl;
import com.ncr.treemanager.service.NodeMapper;
import com.ncr.treemanager.service.dto.NodeDto;

/**
 * @author drgeb
 *
 */
public class TestNodeMapper {


  NodeMapper mapper;

  @Before
  public void setup() {

  }

  @Test
  public void shouldMapNodeToNodeDto() {

    String name = "Test name";
    String description = "Test Description";
    String data = "Test Data";
    // given
    Node node = new NodeImpl(name, description, data);

    // when
    NodeDto nodeDto = mapper.nodeToNodeDto(node);

    // then
    assertThat(nodeDto).isNotNull();
    assertThat(nodeDto.getName()).isEqualTo(name);
    assertThat(nodeDto.getDescription()).isEqualTo(description);
    assertThat(nodeDto.getData()).isEqualTo(data);
  }

  @Test
  public void shouldMapNodeToNodeDto2() {

    String name = "Test name";
    String description = "Test Description";
    String data = "Test Data";
    // given
    Node node = new NodeImpl(name, description, data);

    Node child = new NodeImpl("Child", "Child Description", "Child Data");
    node.addChild(child);

    // when
    NodeDto nodeDto = mapper.nodeToNodeDto(node);

    // then
    assertThat(nodeDto).isNotNull();
    assertThat(nodeDto.getName()).isEqualTo(name);
    assertThat(nodeDto.getDescription()).isEqualTo(description);
    assertThat(nodeDto.getData()).isEqualTo(data);
  }

}
