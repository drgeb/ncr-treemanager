/**
 * 
 */
package com.ncr.treemanager.unit.model;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.ncr.treemanager.model.Node;
import com.ncr.treemanager.model.NodeImpl;
import com.ncr.treemanager.model.NodeManager;

/**
 * @author drgeb
 *
 */
public class TestNodeObject {
  NodeManager nm = null;

  @Before
  public void setUp() {
    nm = NodeManager.getInstance();
  }

  @After
  public void tearDown() {
    nm.reset();
  }

  @Test
  public void testCreateNode() {
    Node root = nm.getRootNode();
    assertTrue(root.getName().equals(Node.ROOT_NODE_NAME));
    assertTrue(root.getDescription().equals(Node.ROOT_NODE_DESCRIPTION));
    NodeImpl child = new NodeImpl("child A", "test child A creation", null);
    try {
      nm.addNode(root, child);
      nm.removeNode(child);
    } catch (Exception e) {
      fail("Should not see an exception" + e);
    }
  }

  @Test
  public void testCreateNode2() {
    Node root = nm.getRootNode();
    assertTrue(root.getName().equals(Node.ROOT_NODE_NAME));
    assertTrue(root.getDescription().equals(Node.ROOT_NODE_DESCRIPTION));
    for (int i = 0; i < Node.MAX_NODE_SIZE; i++) {
      try {
        String name = "child" + i;
        NodeImpl child = new NodeImpl(name, "description for: " + name, null);
        nm.addNode(root, child);
      } catch (Exception e) {
        fail("Should not see an exception" + e);
      }
    }
    for (int i = 0; i < Node.MAX_NODE_SIZE; i++) {
      try {
        String name = "child" + i;
        nm.removeNode(name);
      } catch (IndexOutOfBoundsException e) {
        fail("Should not see an exception" + e);
      }
    }
  }

  @Test
  public void testExceptionCreateNodeTwice() {
    Node root = nm.getRootNode();
    assertTrue(root.getName().equals(Node.ROOT_NODE_NAME));
    assertTrue(root.getDescription().equals(Node.ROOT_NODE_DESCRIPTION));
    String name = "child A";
    NodeImpl childA = new NodeImpl(name, "test child A creation", null);
    NodeImpl childB = new NodeImpl(name, "test child A creation", null);

    nm.addNode(root, childA);
    Throwable e = null;
    try {
      nm.addNode(root, childB);
    } catch (Throwable t) {
      e = t;
    }
    assertTrue(e instanceof RuntimeException);
    assertTrue(e.getMessage().startsWith("Node " + name + " is not unique"));
  }

  @Test
  public void testcreateMoreThanMaxNodeSize() {
    Node root = nm.getRootNode();
    assertTrue(root.getName().equals(Node.ROOT_NODE_NAME));
    assertTrue(root.getDescription().equals(Node.ROOT_NODE_DESCRIPTION));
    Throwable e = null;
    for (int i = 0; i < Node.MAX_NODE_SIZE + 1; i++) {
      try {
        String name = "child" + i;
        NodeImpl child = new NodeImpl(name, "description for: " + name, null);
        nm.addNode(root, child);
      } catch (Throwable t) {
        e = t;
      }
    }
    assertTrue(e instanceof RuntimeException);
    assertTrue(
        e.getMessage().startsWith("Exceeded number of allowed children: " + Node.MAX_NODE_SIZE));
  }

  @Test
  public void testTryToCreateNodeWithRootName1() {
    Node root = nm.getRootNode();
    assertTrue(root.getName().equals(Node.ROOT_NODE_NAME));
    assertTrue(root.getDescription().equals(Node.ROOT_NODE_DESCRIPTION));
    Throwable e = null;
    try {
      nm.addNode(root, root);
    } catch (Throwable t) {
      e = t;
    }
    assertTrue(e instanceof RuntimeException);
    assertTrue(e.getMessage().startsWith("Node root is not unique"));
  }

  @Test
  public void testTryToCreateNodeWithRootName2() {
    Node root = nm.getRootNode();
    assertTrue(root.getName().equals(Node.ROOT_NODE_NAME));
    assertTrue(root.getDescription().equals(Node.ROOT_NODE_DESCRIPTION));
    NodeImpl child = new NodeImpl(Node.ROOT_NODE_NAME, Node.ROOT_NODE_DESCRIPTION, "Root Data");
    Throwable e = null;
    try {
      nm.addNode(root, child);
    } catch (Throwable t) {
      e = t;
    }
    assertTrue(e instanceof RuntimeException);
    assertTrue(e.getMessage().startsWith("Node root is not unique"));
  }

  @Test
  public void testTrytoDeleteRootNode() {
    Node root = nm.getRootNode();
    Throwable e = null;
    try {
      nm.removeNode(root);
    } catch (Throwable t) {
      e = t;
    }
    assertTrue(e instanceof RuntimeException);
    assertTrue(e.getMessage().equals("Not allowed to delete root node!"));
  }
}
