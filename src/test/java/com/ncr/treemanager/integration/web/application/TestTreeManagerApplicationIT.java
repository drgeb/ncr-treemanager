package com.ncr.treemanager.integration.web.application;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ncr.treemanager.model.Node;
import com.ncr.treemanager.service.dto.NodeDto;
import com.ncr.treemanager.web.application.NodeControllerAPI;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

// @RunWith(SpringJUnit4ClassRunner.class)
// @SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes =
// TreeManagerApplication.class)
// @WebAppConfiguration

public class TestTreeManagerApplicationIT {

  public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
  OkHttpClient client;

  @Before
  public void setUp() {
    client = new OkHttpClient();
  }

  @After
  public void tearDown() {
    ;
  }

  String doGetRequest(String url) throws IOException {
    Request request = new Request.Builder().url(url).build();

    Response response = client.newCall(request).execute();
    return response.body().string();
  }

  String doPostRequest(String url, String json) throws IOException {
    RequestBody body = RequestBody.create(JSON, json);
    Request request = new Request.Builder().url(url).post(body).build();
    Response response = client.newCall(request).execute();
    return response.body().string();
  }

  String doPutRequest(String url, String json) throws IOException {
    RequestBody body = RequestBody.create(JSON, json);
    Request request = new Request.Builder().url(url).put(body).build();
    Response response = client.newCall(request).execute();
    return response.body().string();
  }

  private String doDeleteRequest(String url) throws IOException {
    Request request = new Request.Builder().url(url).delete().build();
    Response response = client.newCall(request).execute();
    return response.body().string();
  }

  // @Test
  public void testSimpleGETRootNode() {
    String url = null;
    String result = null;
    try {
      // GET
      url = "http://localhost:8080" + NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE
          + NodeControllerAPI.NODE_REST_ENDPOINT + "/root";
      result = doGetRequest(url);

      // Validate Return Result
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      NodeDto obj = mapper.readValue(result, NodeDto.class);
      assertTrue(obj.getName().equals(Node.ROOT_NODE_NAME));
      assertTrue(obj.getDescription().equals(Node.ROOT_NODE_DESCRIPTION));
      assertTrue(obj.getData() == null);
    } catch (IOException e) {
      fail("Not expecting an exception: " + e.toString());
    }
  }

  // @Test
  public void testCreateNewNodeAndValidate() {
    String url = null;
    String result = null;
    try {
      String name = "N1";
      String description = "description";
      String data = "data 1";

      NodeDto newNode = NodeDto.builder().name(name).description(description).data(data).build();
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
      String json = mapper.writeValueAsString(newNode);

      // PUT Node
      url = "http://localhost:8080" + NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE
          + NodeControllerAPI.NODE_REST_ENDPOINT + "/root";
      result = doPutRequest(url, json);
      // Validate Return Result
      NodeDto createdNode = mapper.readValue(result, NodeDto.class);

      assertTrue(createdNode.getName().equals(name));
      assertTrue(createdNode.getDescription().equals(description));
      assertTrue(createdNode.getData().equals(data));

      // DELETE Node
      url = "http://localhost:8080" + NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE
          + NodeControllerAPI.NODE_REST_ENDPOINT + "/" + name;
      result = doDeleteRequest(url);

      // Get AllDecendants from root size should be 0
      url = "http://localhost:8080" + NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE
          + NodeControllerAPI.CHILDREN_REST_ENDPOINT + "/root";
      result = doGetRequest(url);

      // List <NodeDto> childrenNodes = mapper.readValue(result,
      // NodeDto.class);
    } catch (IOException e) {
      fail("Not expecting an exception: " + e.toString());
    }
  }

  // @Test
  public void testCreateMAXNodesAndValidateChilren() {
    String url = null;
    String result = null;
    try {
      for (int i = 0; i < Node.MAX_NODE_SIZE; i++) {
        String name = "N" + i;
        String description = "description" + i;
        String data = i + " data " + i;

        NodeDto newNode = NodeDto.builder().name(name).description(description).data(data).build();
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String json = mapper.writeValueAsString(newNode);

        // Test PUT
        url = "http://localhost:8080" + NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE
            + NodeControllerAPI.NODE_REST_ENDPOINT + "/root";
        result = doPutRequest(url, json);
        // Validate Return Result
        NodeDto createdNode = mapper.readValue(result, NodeDto.class);

        assertTrue(createdNode.getName().equals(name));
        assertTrue(createdNode.getDescription().equals(description));
        assertTrue(createdNode.getData().equals(data));
      }
      // Test GET
      url = "http://localhost:8080" + NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE
          + NodeControllerAPI.NODE_REST_ENDPOINT + "/root";
    } catch (IOException e) {
      fail("Not expecting an exception: " + e.toString());
    }
  }

  @Test
  public void testGetRootAncestorsResource() {
    String url = null;
    String result = null;
    String rootName = null;
    try {
      for (int i = 0; i < 3; i++) {
        for (int j = 0; j < Node.MAX_NODE_SIZE; j++) {
          String name = "N" + i + "_" + j;
          String description = "description" + i + "_" + j;
          String data = i + " data " + i + "_" + j;
          NodeDto newNode =
              NodeDto.builder().name(name).description(description).data(data).build();
          ObjectMapper mapper = new ObjectMapper();
          mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
          String json = mapper.writeValueAsString(newNode);

          // Test PUT

          if (i == 0) {
            rootName = "root";
          } else {
            rootName = "N" + (i - 1) + "_" + j;
          }
          url = "http://localhost:8080" + NodeControllerAPI.TREEMANAGER_MAIN_RESOURCE
              + NodeControllerAPI.NODE_REST_ENDPOINT + "/" + rootName;

          result = doPutRequest(url, json);
          // Validate Return Result
          NodeDto createdNode = mapper.readValue(result, NodeDto.class);

          assertTrue(createdNode.getName().equals(name));
          assertTrue(createdNode.getDescription().equals(description));
          assertTrue(createdNode.getData().equals(data));
        }
      }
    } catch (IOException e) {
      fail("Not expecting an exception: " + e.toString());
    }
  }

  @Test
  public void testDeleteNodeResource() {

  }

  @Test
  public void testPutNodeResource() {

  }
}
