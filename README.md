# Problem Statement:
Design and implement a RESTful web service which manages an arbitrarily structured tree or hierarchy of nodes.
A node in the tree should have the following mandatory properties:

- A globally unique name
- Brief description

Any additional properties on a node are up to the developer. 
A node in the tree can have a parent node, and can have 0 to 15 children (an attempt to add more than 15 child nodes to a parent will result in an error). 
The tree managed by the service will have a single, parent node, named ‘root’, that will always exist and can never be modified or deleted.
Functional Requirements:
The service will provide resources to support the following:

1. Add a node to the tree at a specific location (for instance, add a new node to a leaf node’s children)
2. Retrieve a single node
3. Retrieve the immediate children of a node
4. Retrieve all descendants of a node (immediate children and nested children)
5. For an arbitrary node, retrieve all ancestors/parents of the node (the path from the root node to the specific node).
6. Remove a node from the tree (also removes all of its children)

Non-Functional Requirements:

1. The sample project must include instructions for building and running the project.  Preferably, projects should use a build tool such as Maven, Gradle, Make, etc.
2. The service should handle any error conditions (such as invalid input or internal errors) with suitable HTTP error responses. 
3. The developer is responsible for designing the API signatures, including the input/output data structures, and any exceptions deemed necessary.
4. Although Java is preferred, the choice of language and frameworks is at the discretion of the developer.  Ideally, the application will run as a simple process/executable, and not require an external container or web server to run.
5. Projects can be submitted to us either via a zip/tarball containing all source, or alternatively, a link to an available GitHub, Bitbucket, or similar repository.

# The Design considerations!
There are so many aspects in the IT industry and computer science that one needs to consider.
For this project these are the topics I have deeply thought about during the creation of this application. They are:

* The Build and Version Control System.
* The framework used to build the application.
* The composition of the application.
* The design for each of the layers in question:
	* The View Layer/API.
	* The Service Layer.
	* The Data or Model layer.
* Testing strategies.
* Future Improvements and Considerations. 

In the next sections I will briefly try to address each of these and explain the reasons why I have made certain decisions in the design or 3rdparty frameworks used.

# The Build and Version Control System.

Well you are aleady here at BitBucket :) so [Git](http://en.wikipedia.org/wiki/Git) is the version control system (VCS) that I have chosen to use. There so many things that I love about Git. To mention a few, it just works! and it allows  me to use it on the Go when traveling either to a Client or for fun!

During this endeavor I did create one branch to investigate the addition of HATEOAS into my Rest API View Layer, but I will go into some details on this later. But this is also a reason why I like Git, it simply allows one to stash away files at any point in time, branch of, and later merge the code back from branch to branch! (This covers the Non-Functional Requirement number 5 :) )

#Initial Problem Interpretation and steps.
To satisfy the non-functional requirement step 4, I have decided to use
the spring boot framework. There are many advantages that Spring has over others but mainly it is the speed at which one can setup an application from end-to-end and include many production and development concerns that really sets it apart. 

For this problem my main concern is for the application to run as simple as possible from the start. The sections below go into more details and present the tasks and steps I took to build the application.

### Task 1: Create a Spring project using spring initializer.
I performed this by going to [Spring Initializer BootStrap](https://start.spring.io) web site.
 
Below is a screen copy of my settings which I selected for the following reasons:
 * I used project Lombok to reduce boiler plate code.
 * Web to automate startup 
 * Actuator helps in production and testing but we need to add security later on around this to protect all the endpoints exposed!
  
![Alt text](pics/SpringInitializer.png?raw=true "Title")


### So how do You build and use "The NCR TreeManager" ?

Well I have decided to use maven, which is my favorite automation build tool. Both maven and gradle are excellent choices.

To build and package the application execute maven as follows:
 
```
$ mvn package
```

To run the application either use java directly or use mvn with springboot:

```
$ java -jar target\treemanager-0.0.1-SNAPSHOT.jar

```

```
$ mvn spring-boot:run
```

You might also want to use the useful operating system environment variable:

```
$ export MAVEN_OPTS=-Xmx1024m -XX:MaxPermSize=128
```


I used eclipse as my IDE to ease the import of the project into Eclipse one can execute the following command to build the .project and .classpath files.

```
mvn eclipse:eclipse
```

### A little more detail on the framework and how it maps to the Non-Functional Requirements ?

In order to answer the specific "Non-Functional Requirements" number 4 I decided to use the Java programming language and as my Web and Application Framework Spring Boot.

In the previous section I talked about the fact that I sued maven as my build system. So the fact that I am using Spring Boot is more clearly expressed in the pom.xml file 

```
<parent>
	<groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-parent</artifactId>
   <version>1.5.2.RELEASE</version>
  	<relativePath /> <!-- lookup parent from repository -->
</parent>
```

In addition to SpringBoot I have included the following frameworks, API's and are part of my dependencies.

* Spring Actuator
* Lombok
* Swagger
* MapStruct
* Junit
* HamCrest
* OkHTTP
* Spring Hateoas

I will discuss the reason for using each of the frameworks above in the next chapters.

# The Design.

##Division of work
The application is packaged as one complete jar (once the package goal/phase is completed it is vailable in the target directory as treemanager-0.0.1-SNAPSHOT.jar).

I could have created modules for each of the specific layers but this is not needed as far as the non-technical requirements are concerned,

The implementation for the application is divided up into 3 layers.
A model layer, a service layer and the actual boundary.
The corresponding package structure for these 3 areas are
1. com.ncr.treemanager.model
2. com.ncr.treemanager.service
3. com.ncr.treemanager.web.application

One thing that is always important is the actual names that are used in ones code. You could argue that the com.ncr.treemanager.web.application should be com.ncr.treemanager.resource.boundary this is all a bit subjective but at the same time important. Since this is a complete application I decided to use web.application.

### Model
For the model layer I have used an [Composite Pattern](https://en.wikipedia.org/wiki/Composite_pattern) where the Leaf is the NodeImpl class, the Component the Node interface and the Composite the NodeManager class. The NodeManager class is also a Singleton and manages the lifecycle of all the Nodes created. Since persistence is not a requirement this was not implemented, but having the code laid out the way it is, this could be easily extended to this layer.

Inorder to reduce the amount of boilerplate code and speedup development I have used a 3rdparty project called [Lombok](https://projectlombok.org/). I have specifically used this in my model and DTO classes called Node and NodeDto.
The Annotations used were @Data, @Builder and @AllArgsConstructor.
For the @Data not only are the getters and setter methods automatically generated but the equals, hashCode and toString are also overridden. 

### Service
In order to have a clear separation between the model and other layers such as controller and view, I decided to use the concept of a Data Transfer Object. This is also known as a DTO. Instead of exposing the model objects directly to the rest of the application and converting these into DTO several advantages are gained that I believe are warranted. For example:
	
* Clare definition of which attribute are exposed.
* Security (Hiding internal attributes from the rest of the application)
* Ease of documentation.
* Ease of aggregating from multiple models or data sources.
	
[A more detailed discussion of the pro's and con's for this can be found at this StackOverflow discussion page.](http://stackoverflow.com/questions/36174516/rest-api-dtos-or-not)

To ease the implementation and mapping of the DTO's the project called [MapStruct](http://mapstruct.org) is used. MapStruct basically allows one to map between different object models. What sets MapStruct apart from other mapping frameworks is that it generates mappings at compile-time.

In order to document the [Restful](https://en.wikipedia.org/wiki/Representational_state_transfer) API and make it easy to use and test, I decided to use Swagger. Since I am using a DTO's to expose the data, Swagger only needs to become aware of these classes. Additional concerns and using annotation to hide attributes is therefore not necessary anymore. I have added some screen shots of [Swagger pages](## Screenshots of index and swagger pages) below. The url endpoint for swagger once the application starts is "/swagger-ui.html". 

### Web Application / Rest EndPoint API 
The table below summarizes the rest end point names, which correspond to the technical specifications. 

Spec # | Brief Summary | HTTP Verb | API EndPoint | Operation
-------|---------------|-----------|--------------|---------|---------|
1 | Add a node| PUT | /treemanager/api/node/{name} | addNodeToTree |
2 | Retrieve a single node | GET | /treemanager/api/node/{name} | getSingleNode |
3 | Retrieve the immediate children of a node | GET | /treemanager/api/children/{name} | getImmediateChildren
4 | Retrieve all descendants | GET | /treemanager/api/descendants/{name} | getDescendants |
5 | Retrieve all ancestors/parents | GET | /treemanager/api/ancestors/{name} | getAncestors |
6 | Remove a node | DELETE | /treemanager/api/node/{name} | removeNode |

For usability I believe its best for some of the end points also have a href link to either themselves or to some of aspects defined by the technical specifications.
This is accomplished by using [HATEOAS](https://en.wikipedia.org/wiki/HATEOAS)  design principle. Since I am using spring this has already been solved and therefore I used the Spring HATEOAS implementation. The implementation and extention of the Controler to Handle HATEOAS comes from using ControllerLinkBuilder class provided by the "Spring HATEOAS" package.
In addition since I decided to only add these hrefs to the views for data when a  single node is displayed the implementation of this is considerably condensed into one method andded to the RestControler class.

### Exception Handling.

For the exceptions that occur at the model, service and application layer, I have gone ahead and created a package com.ncr.treemanager.exception.
There are currently 4 Runtime Exceptions for the exceptions that can be thrown in the model classes NodeManager and NodeImpl.

They are listed in the table below:

Runtime Exception  | Exception Code
------------- | -------------
DeleteRootNodeException | "Not allowed to delete root node!"
IllegalNodeArgumentException  | "Node is not unique, node argument: name with value:{name} already exists"
MaxChildenArraySizeExceeded  | "Exceeded number of allowed children: 15"
NodeNotFoundException  | "could not find node {name}."

These exceptions are then caught by using the NodeRestControllerAdvice class which is annotated to be a @RestControllerAdvice. Using AOP if an Exception occurs during processing of a web request this is intercepted, processed and wrapped and exposed back to the client. To handle general exceptions that may occur now or future if the application is extended I created an abtract BaseException class and this acts as the handler for any general Throwable exception that may occure.
The general format for presenting exceptions by the Rest interface in Json is managed by the TreeManagerExceptionJSONInfo class. For this implementation a Throwable exception, message and cause are the attributes that I have implemented. Addition of timestamp or server where the issue is observed can easily be added to this.


# Where to goto once the Application starts.

To make it easy to find the links for the application, I have created an index.html page. Details and screenshots for these are shown in the next section below.
**Note this page is hardcoded to only work with the default configured port 8080 and the server being available as localhost.

Once the application is running this page can be loaded by going to the main url: 

```
main test page	http://localhost:8080/index.html
```

![Alt text](pics/index.png?raw=true "Index Page")

From here you can quickly test and see the results for the hard coded "root" node.

The Swagger link will show the swagger-ui main page:

![Alt text](pics/swagger-index.png?raw=true "Swagger Index Page")

once you click on "List Operations" you will see the following screen:

![Alt text](pics/swagger-ui.png?raw=true "Swagger UI Page")


Clicking further on "Expand Operations" you will see the following screen:

![Alt text](pics/swagger-expanded-operations.png?raw=true "Swagger Expanded Operations")

"PUT" operation the display will show:

![Alt text](pics/swagger-put.png?raw=true "Swagger PUT Page")

## Testing
The tests are divided into two sections at the moment: Unit and Integration.
They are all contained in the same src folder and structured based on the java packaging structure as:

```
com.ncr.treemanager.integration.web.application
com.ncr.treemanager.unit.model
com.ncr.treemanager.unit.service;
```

I have created this packaing scheme to make it simple to track what type of test it is and what specifically the tests inside the java class is testing.
Currently I only have 4 Test classes.

Test Class Name| Test package structure|
---------------|-----------------------|
TestTreeManagerApplicationIT|com.ncr.treemanager.integration.web.application|
TestNodeMapper|com.ncr.treemanager.unit.service|
TestNodeObject |com.ncr.treemanager.unit.model|
TestNodeService|com.ncr.treemanager.unit.service|

### Unit Tests
From the table above you can see that there are 3 unit tests. I have not been able to complete all the tests that I want but at least you can see my intent. More importantly my thought and organization of the work for comming up with a TestStrategy.
The most important test cases however are TestNodeMapper which I have started to implement. I usually like dividing this up into two groups: Postive and Negative Tests. I also like to immediately start with the missiion crtical postive tests. These tests concentrate on testing the most important business critical pathways of the application where the outcome must be known. So the assertion is on a postive completion of some spectific set of tasks. What i call the negative tests are all the cases where an exception, fault handeling or default behviour is observed when the application or API is instrumented with data that causes this.

### Integration Tests
To run the integration tests you have to start the application first.
Once the application is up and running using either the IDE or mvn one can execute the tests packaged inside the com.ncr.treemanager.integration package.
Currently I only have one test class called TestTreeManagerApplicationIT.

### Manual Tests
I performed the following manual tests using the following tools:

1. Use [PostMan](https://www.getpostman.com) and test GET, PUT, DELETE operations for "The NCR TreeManager"
2. Repeat using [SOAPUI](https://www.soapui.org) (just another test tool I also use)
3. Use simple [Chrome browser](https://www.google.com/chrome/) (See if I can travese the links inside HATEOAS easily. This truely gives a feel from usability of the API.)

Below are different screen captures during testing phase using the tools mentioned above.
 
![Alt text](pics/PostMan_addNodeToTree.png?raw=true "PostMan addNodeToTree Test")**Figure: PostMan Test addNodeToTree.**


![Alt text](pics/PostMan_getDescendants.png?raw=true "PostMan getDescendants")
**Figure: PostMan Test getDescendants.
**

![Alt text](pics/PostMan_getSingleNode.png?raw=true "PostMan getSingleNode")
**Figure: PostMan Test getSingleNode.**


![Alt text](pics/PostMan_getAncestors.png?raw=true "PostMan getAncestors")
**Figure: PostMan Test getAncestors.**


![Alt text](pics/PostMan_getImmediateChildren.png?raw=true "PostMan getImmediateChildren")
**Figure: PostMan Test getImmediateChildren.**


![Alt text](pics/PostMan_removeNode.png?raw=true "PostMan removeNode")
**Figure: PostMan Test removeNode.**


##Configuration Details
The configuration of the Application is managed by editing the key value pairs inside the application.properties file.

This is located at the following relative path src/main/resources/application.properties.

Below is a copy of the application.properties. 
The two main key value pairs I want to point out are the server.port and endpoints.enabled. 
If you would like to start the server on a different port number this can be simply changed by modifying the value for server.port.
The endpoints.enabled are set to true enabling all Actuator defined endpoints to become also active and available.

```
# See: https://spring.io/guides/gs/actuator-service 
server.port: 8080

# Production Ready settings set to all sensative off
# so we can see what's going on!
# See: http://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-monitoring.html
security.user.name=admin
security.user.password=secret
management.security.roles=SUPERUSER
management.context-path=/manage
management.port=8081
management.address: 127.0.0.1
management.security.enabled=false

endpoints.enabled=true
endpoints.info.enabled=true

#endpoints.beans.id=springbeans
#
#You can turn on and off the actuators with settings below
endpoints.sensitive=false
endpoints.shutdown.enabled=true
```

# Future Ideas
Other ideas are:	

1. Create a WADL file and expose this.
2. To wrap the Application inside Docker container.
3. Publish the container in AWS.
4. Complete the Unit testing.
5. Create Mock Tests for the service layer and application endpoint.
6. Continue and finish desiging additional Integration Tests.
7. Introduce [CD](https://en.wikipedia.org/wiki/Continuous_delivery) maybe using RedStack Openshift which I am familliar with.
8. Expand the REST API to include other capabilities such as moving a node from one endpoint to another including the underlying structure.
9. Build persistance into the application to one can shutdown the server and restart it from where the user left off. Restarting the server right now simply causes all the data to be lost.
10. Perform simple load testing using Apache JMeter.
11. Create a front-end using either ReactJS or Angular2.
12. Introduce Continuos Delivery. Right now I am focusing on [GOCD](https://www.gocd.io) and learning the [GOCD API](https://api.gocd.io/current) and trying to see how I can turn this into Ansible like tasks for creation of the Pipelines!