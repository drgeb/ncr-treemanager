For spring Basic security make sure the following dependency iss added to your maven project:

 <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-security</artifactId>
    </dependency>


Running the application with the basic setup

This is all that is needed for the first basic application sceleton. When the application ist started, Spring-Boot sets up the security so that the whole application is secured and can only be accessed by a user user; the password is displayed on application startup in the log in a line like this (the password changes each time the application starts). This is the basic behaviour of Spring-Boot when spring-boot-starter-security is found on application startup.
Using default security password: e482e82e-1115-4fc4-86e4-bdcc432da039



If Spring Security is on the classpath then web applications will be secure by default with ‘basic’ authentication on all HTTP endpoints. To add method-level security to a web application you can also add @EnableGlobalMethodSecurity with your desired settings. Additional information can be found in the Spring Security Reference.

The default AuthenticationManager has a single user (‘user’ username and random password, printed at INFO level when the application starts up)
Using default security password: 78fa095d-3f4c-48b1-ad50-e24c31d5cf35


[Note] 



If you fine-tune your logging configuration, ensure that the org.springframework.boot.autoconfigure.security category is set to log INFO messages, otherwise the default password will not be printed.
 

You can change the password by providing a security.user.password. This and other useful properties are externalized via SecurityProperties (properties prefix "security").

The default security configuration is implemented in SecurityAutoConfiguration and in the classes imported from there (SpringBootWebSecurityConfiguration for web security and AuthenticationManagerConfiguration for authentication configuration which is also relevant in non-web applications). To switch off the default web application security configuration completely you can add a bean with @EnableWebSecurity (this does not disable the authentication manager configuration or Actuator’s security). To customize it you normally use external properties and beans of type WebSecurityConfigurerAdapter (e.g. to add form-based login). To also switch off the authentication manager configuration you can add a bean of type AuthenticationManager, or else configure the global AuthenticationManager by autowiring an AuthenticationManagerBuilder into a method in one of your @Configuration classes. There are several secure applications in the Spring Boot samples to get you started with common use cases.

The basic features you get out of the box in a web application are:

•An AuthenticationManager bean with in-memory store and a single user (see SecurityProperties.User for the properties of the user).
•Ignored (insecure) paths for common static resource locations (/css/**, /js/**, /images/**, /webjars/** and **/favicon.ico).
•HTTP Basic security for all other endpoints.
•Security events published to Spring’s ApplicationEventPublisher (successful and unsuccessful authentication and access denied).
•Common low-level features (HSTS, XSS, CSRF, caching) provided by Spring Security are on by default.

All of the above can be switched on and off or modified using external properties (security.*). To override the access rules without changing any other auto-configured features add a @Bean of type WebSecurityConfigurerAdapter with @Order(SecurityProperties.ACCESS_OVERRIDE_ORDER) and configure it to meet your needs.

